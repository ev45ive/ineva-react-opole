inc = (payload = 1, which = "counter") => ({
  type: "INC",
  payload,
  meta: { which }
});
dec = (payload = 1, which = "counter") => ({
  type: "DEC",
  payload,
  meta: { which }
});

var counterReducer = (state = 0, action) => {
  switch (action.type) {
    case "INC":
      return state + action.payload;
    case "DEC":
      return state - action.payload;
    default:
      return state;
  }
};

var filter = (which, reducer) => (state, action) => {
  if (action.meta.which == which) {
    return reducer(state, action);
  }
  return state;
};

[inc(3), dec(), inc(1, "extra")].reduce(
  (state, action) => {
    return {
      counter: filter("counter", counterReducer)(state.counter, action),
      extra_counter: filter("extra", counterReducer)(
        state.extra_counter,
        action
      )
    };
  },
  {
    counter: 0
  }
);
