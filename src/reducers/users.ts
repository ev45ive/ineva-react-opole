import { Dispatch } from "react-redux";
import { Action, ActionCreator, Reducer, combineReducers } from "redux";
import { createSelector } from "reselect";
// http://jsonplaceholder.typicode.com/users/1

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Addres;
  phone: string;
  website: string;
  company: Company;
}
type Entities<T extends { id: number }> = {
  [id: number]: T;
};
type List<T extends { id: number }> = T["id"][];

export interface UsersState {
  entities: Entities<User>;
  list: List<User>;
  selected: number;
}

export const entities: Reducer<Entities<User>> = (
  state = {},
  action: Actions
) => {
  switch (action.type) {
    case "LOAD_USERS":
      return action.payload.reduce(
        (state: Entities<User>, user: User) => {
          state[user.id] = user;
          return state;
        },
        { ...state }
      );
    default:
      return state;
  }
};

export const list: Reducer<List<User>> = (state = [], action: Actions) => {
  switch (action.type) {
    case "LOAD_USERS":
      return [...action.payload.map(user => user.id)];
    default:
      return state;
  }
};

export const selected: Reducer<User["id"]> = (state = -1, action: Actions) => {
  switch (action.type) {
    case "SELECT_USER":
      return action.payload;
    default:
      return state;
  }
};

export const users = combineReducers({
  entities,
  list,
  selected
});
// type UsersType = ReturnType<typeof users>

///////
type Actions = LOAD_USERS | SELECT_USER;

interface SELECT_USER extends Action {
  type: "SELECT_USER";
  payload: User["id"];
}

export const selectUser: ActionCreator<SELECT_USER> = (payload: number) => ({
  type: "SELECT_USER",
  payload
});

interface LOAD_USERS extends Action {
  type: "LOAD_USERS";
  payload: User[];
}

export const loadUsers: ActionCreator<LOAD_USERS> = (payload: User[]) => ({
  type: "LOAD_USERS",
  payload
});

window["loadUsers"] = loadUsers;

export const fetchUsers = () => (dispatch: Dispatch<any>) => {
  const url = "http://jsonplaceholder.typicode.com/users/";
  dispatch({
    type: "FETCH_USERS_START"
  });
  fetch(url)
    .then(response => response.json())
    .then(
      data => {
        dispatch(loadUsers(data));
      },
      err => {
        dispatch({
          type: "FETCH_USERS_FAILED"
        });
      }
    );
};

//////

export const getUsers = createSelector<
  UsersState,
  List<User>,
  Entities<User>,
  User[]
>(
  state => state.list,
  state => state.entities,
  (list, entities) => list.map(id => entities[id])
);

export const getSelectedUser = (state: UsersState) => {
  return state.entities[state.selected] || {};
};

export const getUser = (state:UsersState, userId:number) => {
  return state.entities[userId] || {}
}

//////

export interface Geo {
  lat: string;
  lng: string;
}

export interface Addres {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}
