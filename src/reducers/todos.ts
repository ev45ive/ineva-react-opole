import { Todo } from "../todos/Todo";
import { Action, ActionCreator, combineReducers } from "redux";

export const todosList = (
  state: Todo["id"][],
  action: Actions,
  options: TodosListOptions
) => {
  switch (action.type) {
    case "ADD_TODO": {
      return [...state, action.payload.id];
    }
    case "REMOVE_TODO": {
      return state.filter(id => id != action.payload);
    }
    // case "DELETE_TODO": {
    //   return state.filter(id => id != action.payload);
    // }
    case "MOVE_TODO": {
      if (action.payload.from == options.name) {
        return state.filter(id => id != action.payload.todo.id);
      }
      if (action.payload.to == options.name) {
        return [...state, action.payload.todo.id];
      }
      return state;
    }
    default:
      return state;
  }
};

type Actions = ADD_TODO | REMOVE_TODO | TOGGLE_TODO | MOVE_TODO;

interface ADD_TODO extends Action {
  type: "ADD_TODO";
  payload: Todo;
  meta?: { list: string };
}

interface MOVE_TODO extends Action {
  type: "MOVE_TODO";
  payload: {
    todo: Todo;
    from: string;
    to: string;
  };
}

interface REMOVE_TODO extends Action {
  type: "REMOVE_TODO";
  payload: Todo["id"];
  meta?: { list: string };
}

interface TOGGLE_TODO extends Action {
  type: "TOGGLE_TODO";
  payload: Todo;
}

export const addTodo: ActionCreator<ADD_TODO> = (payload: Todo) => ({
  type: "ADD_TODO",
  payload
});

export const createTodo: ActionCreator<ADD_TODO> = (
  title: Todo["title"],
  list = "current"
) => ({
  type: "ADD_TODO",
  payload: {
    id: Date.now(),
    title,
    completed: false
  },
  meta: {
    list
  }
});

export const removeTodo: ActionCreator<REMOVE_TODO> = (
  payload: Todo["id"],
  list = "current"
) => ({
  type: "REMOVE_TODO",
  payload
});

export const archiveTodo: ActionCreator<MOVE_TODO> = (todo: Todo) => ({
  type: "MOVE_TODO",
  payload: {
    todo,
    from: "current",
    to: "archived"
  }
});

export const toggleTodo: ActionCreator<TOGGLE_TODO> = (payload: Todo) => ({
  type: "TOGGLE_TODO",
  payload
});

window["createTodo"] = createTodo;
window["removeTodo"] = removeTodo;
window["toggleTodo"] = toggleTodo;

interface TodosListOptions {
  name: string;
}

const makeTodosList = (options: TodosListOptions) => {
  return (state: Todo["id"][] = [], action: Actions) => {
    // Filter out actions
    if (action["meta"] && action["meta"].list != options.name) {
      return state;
    }
    return todosList(state, action, options);
  };
};

const todoEntities = (state: { [id: number]: Todo } = {}, action: Actions) => {
  switch (action.type) {
    case "ADD_TODO": {
      const todo = action.payload;
      return {
        ...state,
        [todo.id]: todo
      };
    }
    case "TOGGLE_TODO": {
      const todo = {
        ...action.payload,
        completed: !action.payload.completed
      };
      return {
        ...state,
        [todo.id]: todo
      };
    }
    default:
      return state;
  }
};

export interface TodosState {
  entities: {
    [id: number]: Todo;
  };
  current: Todo["id"][];
  archived: Todo["id"][];
}

export const todos = combineReducers({
  entities: todoEntities,
  current: makeTodosList({ name: "current" }),
  archived: makeTodosList({ name: "archived" })
});

// export const selectList = (state: TodosState, list: string) => {
//   console.log(
//     "gello selector!"
//   );

//   return (state[list] as number[]).map(id => state.entities[id]);
// };

import { createSelector } from "reselect";

export const selectList = (list: string) => {
  return createSelector<TodosState, number[], {}, Todo[]>(
    state => state[list],
    state => state.entities,
    (list, entities) => (list as number[]).map(id => entities[id])
  );
};

export const getCurrentTodos = selectList("current");
export const getArchivedTodos = selectList("archived");
