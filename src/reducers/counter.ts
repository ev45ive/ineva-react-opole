import { Action, ActionCreator } from "redux";

type State = number;

export const counter = (state: State = 0, action: Actions) => {
  switch (action.type) {
    case "INC":
      return state + action.payload;
    case "DEC":
      return state - action.payload;
    default:
      return state;
  }
};

interface INC extends Action {
  type: "INC";
  payload: number;
}

interface DEC extends Action {
  type: "DEC";
  payload: number;
}

type Actions = INC | DEC;

export const inc: ActionCreator<INC> = (payload: number = 1) => ({
  type: "INC",
  payload
});

export const dec: ActionCreator<DEC> = (payload: number = 1) => ({
  type: "DEC",
  payload
});

window['inc'] = inc
window['dec'] = dec