import { connect, Dispatch } from "react-redux";
import { State } from "../store";
import { Todos } from "../todos/Todos";
import {
  createTodo,
  toggleTodo,
  archiveTodo,
  getCurrentTodos
} from "../reducers/todos";
import { Todo } from "../todos/Todo";
import { TodoItem } from "../todos/TodoItem";

const mapStateToProps = (state: State /* , ownProps */) => {
  return {
    todos: getCurrentTodos(state.todos),
    TodoItem: TodoItem
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>) => {
  return {
    onCreateTodo(title: string) {
      dispatch(createTodo(title));
      // dispatch(fetchTodos(dispatch))
    },
    onRemoveTodo(todo: Todo) {
      dispatch(archiveTodo(todo));
    },
    onToggleTodo(todo: Todo) {
      dispatch(toggleTodo(todo));
    }
  };
};

export const CurrentTodos = connect(mapStateToProps, mapDispatchToProps)(Todos);
