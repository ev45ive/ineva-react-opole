import { connect } from "react-redux";
import { UsersList } from "../components/UsersList";
import { State } from "../store";
import { getUsers, fetchUsers, selectUser } from "../reducers/users";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";

const mapStateToProps = (state: State) => {
  return {
    users: getUsers(state.users)
  };
};

// const mapDispatchToProps = (dispatch: any) => {
//   return {
//     fetchUsers: () => {
//       dispatch(fetchUsers());
//     },
//     selectUser: (id: number) => {
//       dispatch(selectUser(id));
//     }
//   };
// };

const mapDispatchToProps = (dispatch: any) => ({
  ...bindActionCreators(
    {
      fetchUsers,
      selectUser
    },
    dispatch
  )
});

const connected = connect(mapStateToProps, mapDispatchToProps)(UsersList);

export const Users = withRouter(connected);
