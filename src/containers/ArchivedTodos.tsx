import { connect, Dispatch } from "react-redux";
import { State } from "../store";
import { Todos } from "../todos/Todos";
import {
  createTodo,
  removeTodo,
  toggleTodo,
  getArchivedTodos
} from "../reducers/todos";
import { Todo } from "../todos/Todo";
// import { ArchiveItem } from '../todos/ArchiveItem';
import { TodoItem } from "../todos/TodoItem";

const mapStateToProps = (state: State /* , ownProps */) => {
  return {
    todos: getArchivedTodos(state.todos),
    TodoItem: TodoItem
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>) => {
  return {
    onCreateTodo(title: string) {
      dispatch(createTodo(title));
    },
    onRemoveTodo(todo: Todo) {
      dispatch(removeTodo(todo.id));
    },
    onToggleTodo(todo: Todo) {
      dispatch(toggleTodo(todo));
    }
  };
};

export const ArchivedTodos = connect(mapStateToProps, mapDispatchToProps)(
  Todos
);
