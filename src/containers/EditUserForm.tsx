import * as React from "react";
import { connect } from "react-redux";
import { InjectedFormProps, reduxForm } from "redux-form";
import { UserForm } from "../components/UserForm";
import { User, getUser } from "../reducers/users";
import { State } from "../store";

export const EditForm = reduxForm<User>({
  form: "edit_user",
  initialValues: {},
  validate,
  enableReinitialize: true
})(UserForm);

export const EditUserForm = connect<
  {
    initialValues: User;
  },
  {},
  {
    userId: number;
  }
>(
  (state: State, ownProps) => {
    return {
      initialValues: getUser(state.users, ownProps.userId)
    };
  },
  (dispatch: any) => ({
    // onSubmit(data){ dispatch(saveUser(data)....
  })
)(props => {
  return props.initialValues.id ? (
    <EditForm initialValues={props.initialValues} />
  ) : (
    <p>Please select User</p>
  );
});

function validate(data: User, state: InjectedFormProps<User, {}>) {
  const errors = {};
  if (!data.name) {
    errors["name"] = "Required";
  }

  if (!data.email) {
    errors["email"] = "Required";
  } else if (!data.email.includes("@")) {
    errors["email"] = "Invalid e-mail";
  }

  return errors;
}
