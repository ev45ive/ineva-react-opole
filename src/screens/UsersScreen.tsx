import * as React from "react";
import { Users } from "../containers/Users";
import { EditUserForm } from "../containers/EditUserForm";
import { Route, RouteComponentProps } from "react-router";

export class UsersScreen extends React.PureComponent {
  render() {
    return (
      <div className="row">
        <div className="col">
          <Users />
        </div>
        <div className="col">
          <Route
            path="/users/:id"
            render={(props: RouteComponentProps<{ id: string }>) => {
              return <EditUserForm userId={parseInt(props.match.params.id)} />;
            }}
          />
        </div>
      </div>
    );
  }
}
