import * as React from "react";
import { Todo } from "../todos/Todo";
import { ThemeToggler } from "../contexts/ThemeContext";
import { CurrentTodos } from "../containers/CurrentTodos";
import { ArchivedTodos } from '../containers/ArchivedTodos';

interface State {
  todos: Todo[];
  archived: Todo[];
}
interface Props {}

export class TodosScreen extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      archived: [],
      todos: []
    };
  }

  archiveTodo = (todo: Todo) => {
    // this.setState(removeFrom('todos',todo))
    // this.setState(addTo('archived',todo))

    this.setState(prevState => ({
      todos: prevState.todos.filter(t => t.id != todo.id),
      archived: [...prevState.archived, todo]
    }));
    if (this.ref) {
      (this.ref as HTMLElement).focus();
    }
  };

  deleteTodo = (todo: Todo) => {
    this.setState(prevState => ({
      archived: prevState.archived.filter(t => t.id != todo.id)
    }));
  };

  handleCreateTodo = (title: string) => {
    const todo: Todo = {
      id: Date.now(),
      title,
      completed: false
    };

    this.setState({
      todos: [...this.state.todos, todo]
    });
  };

  toggleTodo = (todo: Todo) => {
    this.setState(prevState => ({
      todos: prevState.todos.map(t => {
        return t.id == todo.id
          ? {
              ...todo,
              completed: !todo.completed
            }
          : t;
      })
    }));
  };

  ref: React.ReactInstance;

  componentDidUpdate() {
    console.log("refs", this.refs);
  }

  render() {
    return (
      <div className="row">
        <div className="col">
          <CurrentTodos />

          <ThemeToggler />
        </div>
        <div className="col">
          <ArchivedTodos />
        </div>
      </div>
    );
  }
}
