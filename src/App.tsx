import * as React from "react";
import { Route, Redirect, Switch } from "react-router";
import "./App.css";
import { TodosScreen } from "./screens/TodosScreen";
import { UsersScreen } from "./screens/UsersScreen";

interface Props {
  title?: string;
}

class App extends React.Component<Props> {
  public render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col">
            <Switch>
              <Route path="/todos" exact component={TodosScreen} />
              <Route path="/users" component={UsersScreen} />
              <Redirect path="/" exact to="/todos" />
              <Redirect path="*" exact to="/todos" />
            </Switch>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
