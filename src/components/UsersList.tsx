// tsconfig.json: "allowSyntheticDefaultImports": true
import React, { PureComponent } from "react";
import { User } from "../reducers/users";
import { RouteComponentProps } from "react-router-dom";

interface Props extends RouteComponentProps<{ id: string }> {
  users: User[];
  fetchUsers: () => any;
  selectUser: (id: User["id"]) => any;
}

export class UsersList extends PureComponent<Props> {
  state = {};

  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    console.log(this.props)
    return (
      <div>
        {/* <button onClick={this.props.fetchUsers}>Fetch</button> */}
        <div className="list-group">
          {this.props.users.map(user => (
            <div
              className={`list-group-item ${
                parseInt(this.props.match.params.id) == user.id ? "active" : ""
              }`}
              // onClick={() => this.props.selectUser(user.id)}
              onClick={() => this.props.history.push("/users/" + user.id)}
              key={user.id}
            >
              {user.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
