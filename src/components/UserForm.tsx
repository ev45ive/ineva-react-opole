import React, {
  Component,
  InputHTMLAttributes,
  ComponentType,
  SelectHTMLAttributes
} from "react";
import { User } from "../reducers/users";
import {
  InjectedFormProps,
  Field,
  WrappedFieldProps /* GenericFieldHTMLAttributes */,
  FormSection
} from "redux-form";

interface P extends InjectedFormProps<User> {
  // onSubmit: (data: User) => any;
  // initialValues: User;
}

interface S {}

export class UserForm extends Component<P, S> {
  state = {};

  handleSubmit(user: User) {
    console.log(user);
  }
  render() {
    console.log(this.props);

    return (
      <form onSubmit={this.props.handleSubmit(this.handleSubmit)}>
        <Field label="Name" name="name" component={renderField()} />
        <Field label="E-mail" name="email" component={renderField()} />
        <Field label="Phone" name="phone" component={renderField()} />
        <Field
          label="Membership"
          name="membership"
          component={renderField(RenderSelectField)}
        >
          <option value="basic">Basic</option>
          <option value="vip">VIP</option>
        </Field>
        <FormSection name="company">
          <Field label="Name" name="name" component={renderField()} />
        </FormSection>
        <div className="form-group text-right">
          <input type="submit" className="btn btn-success" value="Save" />
        </div>
      </form>
    );
  }
}

const RenderSelectField = (
  props: SelectHTMLAttributes<HTMLSelectElement> & WrappedFieldProps
) => {
  return (
    <select {...props}>
      <option>-- Select Option --</option>
      {props.children}
    </select>
  );
};

const renderField = (
  Type:
    | ComponentType<WrappedFieldProps & P>
    | "input"
    | "select"
    | "textarea" = "input"
) => ({
  label,
  input,
  meta,
  children
}: InputHTMLAttributes<HTMLInputElement> & WrappedFieldProps) => {
  return (
    <div className={`form-group ${meta.active ? "bg-light" : ""}`}>
      <label>{label}</label>
      <Type
        {...input}
        className={`form-control ${((meta.dirty || meta.touched) &&
          (meta.invalid ? "is-invalid" : "is-valid")) ||
          ""}`}
      >
        {children}
      </Type>
      {(meta.dirty || meta.touched) &&
        meta.error && <div className="invalid-feedback">{meta.error}</div>}
    </div>
  );
};
