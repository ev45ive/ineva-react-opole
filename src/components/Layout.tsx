import * as React from "react";
import { NavLink } from "react-router-dom";

export const Layout = (props: { children: React.ReactFragment }) => (
  <React.Fragment>
    <nav className="navbar navbar-expand navbar-dark bg-dark mb-2">
      <div className="container">
        <NavLink className="navbar-brand" to="/" exact>
          React App
        </NavLink>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName="active"
                exact
                to="/todos"
              >
                Todos
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/users">
                Users
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    {/* ================ */}
    <div className="container">
      <div className="row">
        <div className="col">{props.children}</div>
      </div>
    </div>
  </React.Fragment>
);
