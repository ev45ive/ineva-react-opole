import * as React from "react";
import { Todo } from "./Todo";
import { TodoItem } from "./TodoItem";
import { ArchiveItem } from "./ArchiveItem";

interface State {
  newTitle: string;
  counter: number;
}

interface Props {
  todos: Todo[];
  innerRef?: (ref: React.ReactInstance) => void;
  onCreateTodo: (title: string) => void;
  onRemoveTodo: (todo: Todo) => void;
  onToggleTodo: (todo: Todo) => void;
  TodoItem:  typeof TodoItem | typeof ArchiveItem;
}

export class Todos extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      newTitle: "",
      counter: 0
    };
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newTitle: event.target.value
    });
  };

  handleAdd = () => {
    if (!this.state.newTitle) {
      return;
    }
    if (this.props.onCreateTodo) {
      this.props.onCreateTodo(this.state.newTitle);
    }
    this.setState({
      newTitle: ""
    });
  };

  handleEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key == "Enter") {
      this.handleAdd();
    }
  };

  removeTodo = (todo: Todo) => {
    this.props.onRemoveTodo(todo);
  };

  toggleTodo = (todo: Todo) => {
    this.props.onToggleTodo(todo);
  };

  render() {
    const Item = this.props.TodoItem || TodoItem;
    return (
      <React.Fragment>
        {this.props.todos.length ? (
          <div className="list-group">
            {this.props.todos.map(todo => (
              <Item
                key={todo.id}
                todo={todo}
                toggleTodo={this.toggleTodo}
                removeTodo={this.removeTodo}
              />
            ))}
          </div>
        ) : (
          <p>Nothing here ... </p>
        )}
        {this.props.onCreateTodo && (
          <div className="input-group">
            <input
              ref={ref =>
                ref && this.props.innerRef && this.props.innerRef(ref)
              }
              type="text"
              onKeyPress={this.handleEnter}
              value={this.state.newTitle}
              onChange={this.handleChange}
              className="form-control"
            />
            <input
              type="button"
              className="btn"
              onClick={this.handleAdd}
              value="Add"
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}
