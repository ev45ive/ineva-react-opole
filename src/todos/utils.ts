// export const makeCounter = <State extends {}>(
//   fieldName: keyof State,
//   counterName: keyof State
// ) => (prevState: State) => ({
//   [counterName]: prevState[fieldName].length
// });

// interface State{
//   a:number, b:number; c:number
// }

// const state:State = {
//   a:1, b:2, c:3
// }

// function setState(s:Pick<State,'c'>){}

// type PartialS<State> = {
//   [k in keyof State]?: State[k]
// }

// setState({
//   c:4
// })


export function makeCounter<State extends {counter:number}>(field:keyof State){
  return function setState(prevState:State):Pick<State,'counter'>{
    return {
      counter: prevState[field].length
    }
  }
}

// makeCounter<{newTitle:string}>('newTitle')({newTitle, counter:0})