import * as React from "react";
import { Todo } from "./Todo";

interface Props {
  todo: Todo;
  toggleTodo: (todo: Todo) => void;
  removeTodo: (todo: Todo) => void;
}

export class ArchiveItem extends React.Component<Props> {
  render() {
    const { todo, removeTodo } = this.props;
    return (
      <div className="list-group-item">
        <span style={todo.completed?{textDecoration:'line-through'} : {}}>
        {todo.title}
        </span>
        <span className="close" onClick={() => removeTodo(todo)}>
          &times;
        </span>
      </div>
    );
  }
}
