import * as React from "react";
import { Todo } from "./Todo";
import { ThemeConsumer } from "../contexts/ThemeContext";

interface Props {
  todo: Todo;
  toggleTodo: (todo: Todo) => void;
  removeTodo: (todo: Todo) => void;
}

export class TodoItem extends React.Component<Props> {
  render() {
    const { todo, toggleTodo, removeTodo } = this.props;

    return (
      <ThemeConsumer>
        {theme => (
          <div className={`list-group-item ${theme.theme}`}>
            <input
              type="checkbox"
              onChange={() => toggleTodo(todo)}
              checked={todo.completed}
            />
            <span>{todo.title}</span>
            <span className="close" onClick={() => removeTodo(todo)}>
              &times;
            </span>
          </div>
        )}
      </ThemeConsumer>
    );
  }
}
