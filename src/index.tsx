import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
// import "./index.css";
import "bootstrap/dist/css/bootstrap.css";
import registerServiceWorker from "./registerServiceWorker";
import { withTheme } from "./contexts/ThemeContext";
import { Layout } from "./components/Layout";
import { store } from "./store";
import { Provider } from "react-redux";

const ThemedApp = withTheme(App);

import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <Layout>
        <ThemedApp title="React App" defaultTheme="light" />
      </Layout>
    </Provider>
  </Router>,
  document.getElementById("root") as HTMLElement
);

registerServiceWorker();
