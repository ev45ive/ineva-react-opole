import { createStore, combineReducers, applyMiddleware } from "redux";
import { counter } from "./reducers/counter";
import { todos, TodosState } from "./reducers/todos";
import { users, UsersState } from "./reducers/users";
import thunkMiddleware from "redux-thunk";
import { reducer as formReducer, FormReducer } from 'redux-form'

export interface State {
  counter: number;
  todos: TodosState;
  users: UsersState;
  form: FormReducer
}

const rootReducer = combineReducers<State>({
  counter,
  todos,
  users,
  form: formReducer
});


export const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

window["store"] = store;
