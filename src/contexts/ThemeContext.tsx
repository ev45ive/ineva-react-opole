import * as React from "react";

export interface Theme {
  theme: string;
  toggleTheme: () => void;
}

export const ThemeContext = React.createContext({
  theme: "dark",
  toggleTheme: () => {}
});

export const {
  Provider: ThemeProvider,
  Consumer: ThemeConsumer
} = ThemeContext;

export class ThemeToggler extends React.Component {
  render() {
    return (
      <ThemeConsumer>
        {theme => (
          <input type="checkbox" onChange={() => theme.toggleTheme()} />
        )}
      </ThemeConsumer>
    );
  }
}

// type themeHOC = <P>(Component: React.ComponentClass<P>) => React.Component<P>;

export const withTheme = <P extends {}>(Component: React.ComponentClass<P>) => {
  return class WithTheme extends React.Component<
    P & {
      defaultTheme?: string;
    }
  > {
    state = {
      theme: "light"
    };

    toggleTheme = () => {
      this.setState({
        theme: this.state.theme == "light" ? "dark" : "light"
      });
    };

    render() {
      return (
        <ThemeProvider
          value={{
            theme: this.state.theme,
            toggleTheme: this.toggleTheme
          }}
        >
          <Component {...this.props} />
        </ThemeProvider>
      );
    }
  };
};
